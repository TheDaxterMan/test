from bottle import route, run, template


@route('/hello/<name>')
def index(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/multiply/<a>/<b>')
def route_multiply(a, b):
    result = multiply(a, b)
    return template('<b>Result of {{a}} * {{b}} is {{result}}</b>!', a=a, b=b, result=result)


def multiply(a, b):
    return int(a) * int(b)


@route('/users')
def users():
    users_list = ['Alexandre', 'Brenda', 'Pierre', 'etc']
    output = template('template', users=users_list)
    return output


if __name__ == '__main__':
    run(host='localhost', port=8080)
